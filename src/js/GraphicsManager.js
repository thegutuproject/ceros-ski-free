export default class GraphicsManager {
  constructor() {
    this.gameWidth = window.innerWidth;
    this.gameHeight = window.innerHeight;
    this.context = undefined;

    /**
     * Management files to link together the classes
     */

    this.collisionManager = undefined;
    this.assetManager = undefined;
    this.skierManager = undefined;
    this.utilityManager = undefined;
  }

  /**
   * Loads the canvas object using pureJS
   * Also sets the CSS properties
   */
  loadCanvas() {
    let canvas = document.createElement('canvas');
    let parentDiv = document.getElementById('game');
    canvas.id = 'skiFreeGame';
    canvas.width = this.gameWidth * window.devicePixelRatio;
    canvas.height = this.gameHeight * window.devicePixelRatio;
    canvas.style.width = this.gameWidth + 'px';
    canvas.style.height = this.gameHeight + 'px';
    parentDiv.appendChild(canvas);

    this.context = canvas.getContext('2d');
    this.context.scale(window.devicePixelRatio, window.devicePixelRatio);
  }

  /**
   * Removes everything from canvas
   */
  clearCanvas() {
    this.context.clearRect(0, 0, this.gameWidth, this.gameHeight);
  }

  /**
   * Draws obstacles on the canvas and adds them to the obstacles in collisionManager
   */
  drawObstacles() {
    const newObstacles = [];

    _.each(this.collisionManager.obstacles, (obstacle) => {
      const obstacleImage = this.assetManager.loadedAssets[obstacle.type];
      const x = obstacle.x - this.skierManager.skierMapX - obstacleImage.width / 2;
      const y = obstacle.y - this.skierManager.skierMapY - obstacleImage.height / 2;

      if (x < -100 || x > this.gameWidth + 50 || y < -100 || y > this.gameHeight + 50) {
        return;
      }

      this.context.drawImage(obstacleImage, x, y, obstacleImage.width, obstacleImage.height);

      newObstacles.push(obstacle);
    });

    this.collisionManager.obstacles = newObstacles;
  }

  /**
   * Generates random obstacles, and adds them to the obstacles in collisionManager.
   * Then that is used to place random obstacles initially
   * @param minX
   * @param maxX
   * @param minY
   * @param maxY
   */
  placeRandomObstacle(minX, maxX, minY, maxY) {
    const obstacleIndex = _.random(0, this.collisionManager.obstacleTypes.length - 1);

    const position = this.collisionManager.calculateOpenPosition(minX, maxX, minY, maxY);

    const currentObstacles = this.collisionManager.obstacles;

    currentObstacles.push({
      type: this.collisionManager.obstacleTypes[obstacleIndex],
      x: position.x,
      y: position.y
    });

    this.collisionManager.obstacles = currentObstacles;
  }

  /**
   * Placement of new obstacles as skier is "moving"
   * @param direction
   */
  placeNewObstacle(direction) {
    const shouldPlaceObstacle = _.random(1, 8);
    if (shouldPlaceObstacle !== 8) {
      return;
    }

    const leftEdge = this.skierManager.skierMapX;
    const rightEdge = this.skierManager.skierMapX + this.gameWidth;
    const topEdge = this.skierManager.skierMapY;
    const bottomEdge = this.skierManager.skierMapY + this.gameHeight;

    switch (direction) {
      default:
        break;
      case 1: // left
        this.placeRandomObstacle(leftEdge - 40, leftEdge, topEdge, bottomEdge);
        break;
      case 2: // left down
        this.placeRandomObstacle(leftEdge - 40, leftEdge, topEdge, bottomEdge);
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 40);
        break;
      case 3: // down
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 40);
        break;
      case 4: // right down
        this.placeRandomObstacle(rightEdge, rightEdge + 40, topEdge, bottomEdge);
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 40);
        break;
      case 5: // right
        this.placeRandomObstacle(rightEdge, rightEdge + 40, topEdge, bottomEdge);
        break;
      case 6: // up
        this.placeRandomObstacle(leftEdge, rightEdge, topEdge - 40, topEdge);
        break;
    }
  }

  /**
   * Used to initially set the obstacles on the canvas
   */
  placeInitialObstacles() {
    const numberObstacles = Math.ceil(
      _.random(7, 10) * (this.gameWidth / 800) * (this.gameHeight / 500)
    );

    const minX = -50;
    const maxX = this.gameWidth + 40;
    const minY = this.gameHeight / 2 + 80;
    const maxY = this.gameHeight + 40;

    for (let i = 0; i < numberObstacles; i++) {
      this.placeRandomObstacle(minX, maxX, minY, maxY);
    }

    this.collisionManager.obstacles = _.sortBy(this.collisionManager.obstacles, (obstacle) => {
      const obstacleImage = this.assetManager.loadedAssets[obstacle.type];
      return obstacle.y + obstacleImage.height;
    });
  }
}

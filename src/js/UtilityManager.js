export default class UtilityManager {
  constructor() {
    /**
     * Skier states for easier reading
     * @type {{DOWN_RIGHT: number, DOWN: number, LEFT: number, DOWN_LEFT: number, RIGHT: number, JUMP1: number, JUMP4: number, JUMP5: number, JUMP2: number, CRASHED: number, JUMP3: number}}
     */
    this.skierDirection = {
      CRASHED: 0,
      LEFT: 1,
      DOWN_LEFT: 2,
      DOWN: 3,
      DOWN_RIGHT: 4,
      RIGHT: 5,
      JUMP1: 6,
      JUMP2: 7,
      JUMP3: 8,
      JUMP4: 9,
      JUMP5: 10
    };

    /**
     * Keyboard values for event manager
     * @type {{PAUSE: number, DOWN: number, LEFT: number, RIGHT: number, UP: number, JUMP: number}}
     */
    this.keyboardValues = {
      UP: 38,
      LEFT: 37,
      DOWN: 40,
      RIGHT: 39,
      PAUSE: 80, // P
      JUMP: 32 // Space
    };

    /**
     * Spans that represent score locations to be updated and leaderBoard
     * @type {HTMLElement}
     */
    this.currentScoreSpan = document.getElementById('current-score');
    this.highScoreSpan = document.getElementById('high-score');
    this.scoreMultiplierSpan = document.getElementById('score-multiplier');
    this.currentSpeedSpan = document.getElementById('current-speed');
    this.numOfLivesSpan = document.getElementById('num-of-lives');
    this.leaderboardListSpan = document.getElementById('leaderboard-list');

    this.score = 0;
    this.highScore = 0;
    this.leaderBoard = [];
    this.isPaused = true;
    this.gameOver = false;

    /**
     * Management files to link together the classes
     */

    this.skierManager = undefined;
  }

  /**
   * Resets scores to initial state
   * Needed due to resetLeaderBoard
   */
  resetScore() {
    this.score = 0;
    this.highScore = 0;
    this.leaderBoard = [];
  }

  /**
   * Resets the game state to initial state
   */
  resetGameState() {
    this.isPaused = true;
    this.gameOver = false;
  }

  /**
   * Toggle gamePaused game state
   * Also toggles the showing/closing of the menu modal
   */
  togglePaused() {
    $('#menu-modal').modal('toggle');

    this.isPaused = !this.isPaused;
  }

  /**
   * Resets the leaderboardListSpan to nothing by setting inner html to ''.
   * After, it goes through the leaderBoard object and appends them to the span.
   * Had to add 1 to index to make it start from 1
   */
  updateLeaderboard() {
    this.leaderboardListSpan.innerHTML = '';
    if (this.leaderBoard && this.leaderBoard.length !== 0) {
      this.leaderBoard.forEach((score, index) => {
        const listItem = document.createElement('li');
        listItem.innerHTML = listItem.innerHTML + (index + 1) + '. ' + score + ' meters';
        this.leaderboardListSpan.appendChild(listItem);
      });
    }
  }

  /**
   * This is just used to update the internal score variables
   */
  updateAndDisplayScore() {
    const scoreMultiplier = (this.skierManager.skierSpeed - 8) / 10 + 1;
    this.score = Math.round(this.skierManager.skierMapY * scoreMultiplier);

    this.currentScoreSpan.textContent = this.score;
    this.highScoreSpan.textContent = this.highScore;
    this.scoreMultiplierSpan.innerText = ((this.skierManager.skierSpeed - 8) / 10 + 1).toFixed(2);
    this.currentSpeedSpan.textContent = this.skierManager.skierSpeed.toFixed(2);
    this.numOfLivesSpan.textContent = this.skierManager.skierLives;
  }

  /**
   * Resets the leaderboard when user clicks "Reset Leaderboard"
   * If erases the local storage, then resets the leaderboardSpan
   */
  resetLeaderboard() {
    this.clearLocalStorage();
    this.updateLeaderboard();
  }

  /**
   * Stores the leaderBoard object to local storage.
   * It's converted to JSON since we can only store strings
   */
  saveScoreInLocalStorage() {
    if (window && window.localStorage) {
      if (this.score) {
        this.leaderBoard.push(this.score);
        window.localStorage.setItem('ceros_leaderboard', JSON.stringify(this.leaderBoard));
      }
    }
  }

  /**
   * Loads the score from the local storage. If score is not null,
   * it goes through, sorts it and returns up to 5 items (to keep
   * leaderboard from growing too big)
   * Also updates the highScore variable from biggest in leaderBoard.
   */
  getScoreFromLocalStorage() {
    if (window && window.localStorage) {
      const leaderBoard = JSON.parse(window.localStorage.getItem('ceros_leaderboard'));
      if (leaderBoard && leaderBoard.length !== 0) {
        const leaderBoardLength = leaderBoard.length < 5 ? leaderBoard.length : 5;
        this.leaderBoard = leaderBoard
          .sort((a, b) => {
            return b - a;
          })
          .slice(0, leaderBoardLength);
        this.highScore = leaderBoard[0];
      }
    }
  }

  /**
   * Removes the leaderboard values from local storage
   */
  clearLocalStorage() {
    if (window && window.localStorage) {
      window.localStorage.removeItem('ceros_leaderboard');
      this.resetScore();
    }
  }
}

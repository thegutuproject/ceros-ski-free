export default class SkierManager {
  constructor() {
    this.skierDirection = 5;
    this.skierMapX = 0;
    this.skierMapY = 0;
    this.skierSpeed = 8;
    this.skierLives = 5;
    this.skierCanMove = true;
    /**
     * The skierSpeedModifier is just a constant value to
     * modify the speed incrementing, lower means speed goes
     * up faster
     * @type {number}
     */
    this.skierSpeedModifier = 7500;
    /**
     * Management files to link together the classes
     */

    this.assetManager = undefined;
    this.graphicsManager = undefined;
    this.utilityManager = undefined;
    this.rhinoManager = undefined;
  }

  /**
   * Reset skierManager to initial state
   */
  resetSkier() {
    this.skierDirection = 5;
    this.skierMapX = 0;
    this.skierMapY = 0;
    this.skierSpeed = 8;
    this.skierLives = 5;
    this.skierCanMove = true;
  }

  /**
   * Decreases skier direction (pressing left key)
   */
  skierDirectionDecrease() {
    this.skierDirection--;
  }

  /**
   * Increases skier direction (pressing right key)
   */
  skierDirectionIncrease() {
    this.skierDirection++;
  }

  /**
   * Once a collision occurs, we decrease skierLife by 1.
   * Also set the gameOver boolean when skierLives is
   * less than or equal to 0
   */
  skierLoseLife() {
    this.skierLives--;
    this.utilityManager.gameOver = this.skierLives <= 0;
  }

  /**
   * Returns the proper asset based on the skierDirection
   * Also used for jumping assets too
   * @returns {string}
   */
  getSkierAsset() {
    let skierAssetName;
    switch (this.skierDirection) {
      case 0:
        skierAssetName = 'skierCrash';
        break;
      case 1:
        skierAssetName = 'skierLeft';
        break;
      case 2:
        skierAssetName = 'skierLeftDown';
        break;
      case 3:
        skierAssetName = 'skierDown';
        break;
      case 4:
        skierAssetName = 'skierRightDown';
        break;
      case 5:
        skierAssetName = 'skierRight';
        break;
      case 6:
        skierAssetName = 'skierJump1';
        break;
      case 7:
        skierAssetName = 'skierJump2';
        break;
      case 8:
        skierAssetName = 'skierJump3';
        break;
      case 9:
        skierAssetName = 'skierJump4';
        break;
      case 10:
        skierAssetName = 'skierJump5';
        break;
      default:
        break;
    }

    return skierAssetName;
  }

  /**
   * Moves the skier (or rather, updates the
   * skier map coordinates to simulate moving)
   */
  moveSkier() {
    /**
     * Skier doesn't move if the game is paused or if the skier is caught
     * I think the skierCanMove is what causes it to stop before the rhino
     * has fully reached... something with the collisionManager running quicker?
     */
    if (this.utilityManager.isPaused || !this.skierCanMove) {
      return;
    }
    switch (this.skierDirection) {
      default:
        break;
      case this.utilityManager.skierDirection.DOWN_LEFT:
        this.skierMapX -= Math.round(this.skierSpeed / 1.4142);
        this.skierMapY += Math.round(this.skierSpeed / 1.4142);

        this.utilityManager.updateAndDisplayScore();

        this.graphicsManager.placeNewObstacle(this.skierDirection);
        break;
      case this.utilityManager.skierDirection.DOWN:
        this.skierMapY += this.skierSpeed;

        this.utilityManager.updateAndDisplayScore();

        this.graphicsManager.placeNewObstacle(this.skierDirection);

        break;
      case this.utilityManager.skierDirection.DOWN_RIGHT:
        this.skierMapX += this.skierSpeed / 1.4142;
        this.skierMapY += this.skierSpeed / 1.4142;

        this.utilityManager.updateAndDisplayScore();

        this.graphicsManager.placeNewObstacle(this.skierDirection);

        break;

      case this.utilityManager.skierDirection.JUMP1:
      case this.utilityManager.skierDirection.JUMP2:
      case this.utilityManager.skierDirection.JUMP3:
      case this.utilityManager.skierDirection.JUMP4:
      case this.utilityManager.skierDirection.JUMP5:
        this.skierMapY += this.skierSpeed * 0.7;
        this.utilityManager.updateAndDisplayScore();
        break;
    }
  }

  /**
   * Draws the skier and given asset on canvas
   */
  drawSkier() {
    const skierAssetName = this.getSkierAsset();
    const skierImage = this.assetManager.loadedAssets[skierAssetName];
    const x = (this.graphicsManager.gameWidth - skierImage.width) / 2;
    const y = (this.graphicsManager.gameHeight - skierImage.height) / 2;

    this.graphicsManager.context.drawImage(skierImage, x, y, skierImage.width, skierImage.height);
  }

  /**
   * Animation function, generates an interval and runs every 150ms,
   * each time incrementing the direction (to change the asset) and
   * given the canvas rerendering the gameLoop, shows the skier jumping
   */
  jumpSkier() {
    let animationIndex = 6;
    let jumpingInterval = setInterval(() => {
      if (animationIndex < 10) {
        this.skierDirection = animationIndex;
      } else {
        clearInterval(jumpingInterval);
        this.skierDirection = 3;
      }
      animationIndex++;
    }, 150);
  }

  /**
   * Dynamically increases the speed of the skier based on the time
   * the game started. Bootleg safety feature (prevents updating
   * speed when the game is paused, or used is pointed straight
   * left or straight right) However, the time will still increment :/
   * @param startTime
   */
  updateSpeed(startTime) {
    if (this.skierDirection > 1 && this.skierDirection < 5 && !this.utilityManager.isPaused) {
      const endTime = new Date();
      const timeDifference = (endTime - startTime) / 1000; // seconds
      this.skierSpeed += timeDifference / this.skierSpeedModifier;
    }
  }
}

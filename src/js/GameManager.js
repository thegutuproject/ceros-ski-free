import 'bootstrap';
import AssetManager from './AssetManager';
import CollisionManager from './CollisionManager';
import GraphicsManager from './GraphicsManager';
import SkierManager from './SkierManager';
import UtilityManager from './UtilityManager';
import RhinoManager from './RhinoManager';

require('../css/style.less');

$(document).ready(() => {
  /**
   * This is more of a compatibility thing, each browser has a different animationFrame
   */
  let browserAnimationFrame = (() => {
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame
    );
  })();

  /**
   * I'm not fully confident in the following block of code,
   * I'm not fully sure if this is the proper way to do this.
   * Part of me feels like this should not be instantiated this
   * way, and these variables shouldn't be shared amongst each other?
   *
   * Or perhaps I should of added a different method to separate game
   * management(this section) from the actual game initialization
   * @type {AssetManager}
   */
  const assetManager = new AssetManager();
  const collisionManager = new CollisionManager();
  const graphicsManager = new GraphicsManager();
  const skierManager = new SkierManager();
  const utilityManager = new UtilityManager();
  const rhinoManager = new RhinoManager();

  collisionManager.assetManager = assetManager;
  collisionManager.graphicsManager = graphicsManager;
  collisionManager.skierManager = skierManager;
  collisionManager.rhinoManager = rhinoManager;
  collisionManager.utilityManager = utilityManager;

  graphicsManager.assetManager = assetManager;
  graphicsManager.collisionManager = collisionManager;
  graphicsManager.skierManager = skierManager;
  graphicsManager.utilityManager = utilityManager;

  skierManager.assetManager = assetManager;
  skierManager.graphicsManager = graphicsManager;
  skierManager.utilityManager = utilityManager;

  rhinoManager.assetManager = assetManager;
  rhinoManager.graphicsManager = graphicsManager;
  rhinoManager.utilityManager = utilityManager;
  rhinoManager.skierManager = skierManager;
  rhinoManager.collisionManager = collisionManager;

  utilityManager.skierManager = skierManager;

  /**
   * Start time to pass into the dynamic speed increment function
   * @type {Date}
   */
  let startTime = new Date();

  /**
   * Experimented with cancelAnimation, and it needed the animation
   * id, stored in gameAnimation. However, I didn't do this, it
   * turned out to be too difficult.
   */
  let gameAnimation;

  /**
   * Initiates the menu modal, prevents the keyboard ESC key from closing it.
   * It starts open because the game starts out paused
   */
  $('#menu-modal').modal({
    backdrop: 'static',
    keyboard: false,
    show: utilityManager.isPaused
  });

  /**
   * Play button event registration
   * @type {HTMLElement}
   */
  const playButton = document.getElementById('play-button');
  playButton.addEventListener('click', () => {
    utilityManager.togglePaused();
  });

  /**
   * Restart button event registration.
   * New date is used to reset the difficulty.
   */
  const restartButton = document.getElementById('restart-button');
  restartButton.addEventListener('click', () => {
    resetManagers();
    startTime = new Date();
    gameAnimation = requestAnimationFrame(gameLoop);
  });

  /**
   * Reset Leaderboard button event registration.
   * @type {HTMLElement}
   */
  const resetLeaderboardButton = document.getElementById('reset-leaderboard-button');
  resetLeaderboardButton.addEventListener('click', () => {
    utilityManager.resetLeaderboard();
  });

  /**
   * Game loop to update, draw, etc.
   * This might have too many things in it, the game is not as smooth
   * as I'd like it to be
   */
  const gameLoop = () => {
    graphicsManager.context.save();
    graphicsManager.clearCanvas();
    skierManager.moveSkier();
    collisionManager.checkIfSkierHitObstacle();
    skierManager.drawSkier();
    /**
     * If skier score is above 10,000,
     * start introducing the rhino
     */
    if (utilityManager.score >= 10000) {
      rhinoManager.moveRhino();
      collisionManager.checkIfSkierHitRhino();
      rhinoManager.drawRhino();
    }
    graphicsManager.drawObstacles();
    utilityManager.updateAndDisplayScore();
    skierManager.updateSpeed(startTime);

    graphicsManager.context.restore();
    if (!utilityManager.gameOver) {
      gameAnimation = browserAnimationFrame(gameLoop);
    } else {
      /**
       * If the game is over, go here
       */
      utilityManager.updateAndDisplayScore();
      utilityManager.updateLeaderboard();
      utilityManager.saveScoreInLocalStorage();
      utilityManager.togglePaused();
      disablePlayButton();
    }
  };

  /**
   * Keyhandler
   */
  const setupKeyhandler = () => {
    $(window).keydown((event) => {
      switch (event.which) {
        default:
          break;
        case utilityManager.keyboardValues.LEFT: // LEFT
          if (!utilityManager.isPaused) {
            // when skier is not moving, vertically, player can move him left or right
            if (skierManager.skierDirection === 1) {
              skierManager.skierMapX -= skierManager.skierSpeed;
              graphicsManager.placeNewObstacle(skierManager.skierDirection);
              // if the skier is in crashed state, when player moves left or right,
              // skier will begin leftDown or rightDown like original skifree
            } else if (skierManager.skierDirection === 0) {
              skierManager.skierMapX -= skierManager.skierSpeed;
              skierManager.skierDirection = 2;
              // otherwise, change skier direction
            } else {
              skierManager.skierDirectionDecrease();
            }
          }

          event.preventDefault();
          break;
        case utilityManager.keyboardValues.RIGHT: // RIGHT
          if (!utilityManager.isPaused) {
            // when skier is not moving, vertically, player can move him left or right
            if (skierManager.skierDirection === 5) {
              skierManager.skierMapX += skierManager.skierSpeed;
              graphicsManager.placeNewObstacle(skierManager.skierDirection);
              // if the skier is in crashed state, when player moves left or right,
              // skier will begin leftDown or rightDown like original skifree
            } else if (skierManager.skierDirection === 0) {
              skierManager.skierMapX += skierManager.skierSpeed;
              skierManager.skierDirection = 4;
              // otherwise, change skier direction
            } else {
              skierManager.skierDirectionIncrease();
            }
          }

          event.preventDefault();
          break;
        case utilityManager.keyboardValues.UP: // UP
          if (!utilityManager.isPaused) {
            if (skierManager.skierDirection === 1 || skierManager.skierDirection === 5) {
              skierManager.skierMapY -= skierManager.skierSpeed;
              graphicsManager.placeNewObstacle(6);
            }
          }
          event.preventDefault();
          break;
        case utilityManager.keyboardValues.DOWN: // DOWN
          if (!utilityManager.isPaused) {
            skierManager.skierDirection = 3;
            event.preventDefault();
          }
          break;
        case utilityManager.keyboardValues.PAUSE: // P
          if (!utilityManager.gameOver) {
            utilityManager.togglePaused();
          }
          event.preventDefault();
          break;
        case utilityManager.keyboardValues.JUMP: // Space
          skierManager.jumpSkier();
          event.preventDefault();
          break;
      }
    });
  };

  /**
   * This resets the game to default state.
   * This also might be the issue with reported bug 2 and 3
   */
  const resetManagers = () => {
    collisionManager.resetCollisionManager();
    skierManager.resetSkier();
    rhinoManager.resetRhino();
    utilityManager.resetScore();
    utilityManager.resetGameState();
    utilityManager.getScoreFromLocalStorage();
    utilityManager.updateAndDisplayScore();
    utilityManager.updateLeaderboard();
    graphicsManager.placeInitialObstacles();
    utilityManager.togglePaused();
    disablePlayButton();
  };

  /**
   * The play button is disabled once the player is dead,
   * but is enabled when its a regular pause
   */
  const disablePlayButton = () => {
    utilityManager.gameOver
      ? playButton.setAttribute('disabled', true)
      : playButton.removeAttribute('disabled');
  };

  const initGame = () => {
    setupKeyhandler();
    graphicsManager.loadCanvas();
    assetManager.loadAssets().then(() => {
      utilityManager.getScoreFromLocalStorage();
      utilityManager.updateLeaderboard();
      graphicsManager.placeInitialObstacles();

      gameAnimation = browserAnimationFrame(gameLoop);
    });
  };

  initGame(gameLoop);
});

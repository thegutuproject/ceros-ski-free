export default class RhinoManager {
  constructor() {
    this.rhinoDirection = 8;
    this.rhinoAnimation = 16;
    this.rhinoMapX = 0;
    this.rhinoMapY = 0;
    this.rhinoSpeed = 8;
    this.caughtSkier = false;

    /**
     * Buffer to show the rhino approaching the user
     * @type {number}
     */
    this.rhinoBuffer = 140;

    /**
     * Management files to link together the classes
     */

    this.assetManager = undefined;
    this.collisionManager = undefined;
    this.graphicsManager = undefined;
    this.utilityManager = undefined;
    this.skierManager = undefined;
  }

  /**
   * Resets rhinoManager to default state
   */
  resetRhino() {
    this.rhinoDirection = 5;
    this.rhinoAnimation = 16;
    this.rhinoMapX = 0;
    this.rhinoMapY = 0;
    this.rhinoSpeed = 8;
    this.rhinoBuffer = 140;
    this.caughtSkier = false;
  }

  /**
   * Returns the rhino asset based on the animationIndex
   * @returns {string}
   */
  getRhinoAsset() {
    let rhinoAssetName;
    switch (this.rhinoAnimation) {
      case 16:
        rhinoAssetName = 'rhinoDefault';
        break;
      case 17:
        rhinoAssetName = 'rhinoLiftEat1';
        break;
      case 18:
        rhinoAssetName = 'rhinoLiftEat2';
        break;
      case 19:
        rhinoAssetName = 'rhinoLiftEat3';
        break;
      case 20:
        rhinoAssetName = 'rhinoLiftEat4';
        break;
      case 21:
        rhinoAssetName = 'rhinoLiftEat5';
        break;
      case 22:
        rhinoAssetName = 'rhinoLiftEat6';
        break;
      case 23:
        rhinoAssetName = 'rhinoRunLeft1';
        break;
      case 24:
        rhinoAssetName = 'rhinoRunLeft2';
        break;
      default:
        break;
    }

    return rhinoAssetName;
  }

  /**
   * This is a diret copy of the moveSkier method, except with references to rhino.
   * My interpretation of a rhino eating is the rhino coming and starting to chase
   * after the skier, while progressively catching up. However, I wasn't able to
   * implement it so the rhino followed.
   *
   * Either way, the rhino moves according to the skier so it follows him.
   * Unfortunately, it still doesn't quite fully intersect with the skier.
   */
  moveRhino() {
    /**
     * Once this method is reached for the first time, the rhinoX and rhinoY
     * coordinates are updated to the players coordinates. Once they are not 0,
     * the method takes over
     */
    if (this.rhinoMapX === 0 && this.rhinoMapY === 0) {
      this.rhinoMapX = this.skierManager.skierMapX;
      this.rhinoMapY = this.skierManager.skierMapY;
    }

    /**
     * Cant move if the game is paused or the skier has been caught
     */
    if (!this.utilityManager.isPaused || this.caughtSkier) {
      return;
    }
    switch (this.skierManager.skierDirection) {
      default:
        break;
      case 2: //
        this.rhinoMapX -= Math.round(this.skierManager.skierSpeed / 1.4142);
        this.rhinoMapY += Math.round(this.skierManager.skierSpeed / 1.4142);
        break;
      case 3:
        this.rhinoMapY += this.skierManager.skierSpeed;
        break;
      case 4:
        this.rhinoMapX += this.skierManager.skierSpeed / 1.4142;
        this.rhinoMapY += this.skierManager.skierSpeed / 1.4142;
        break;
    }
  }

  /**
   * This changes the rhino animation index between the three "states" of the rhino running.
   * If the gameLoop wasn't being iterated over so fast, it would look a bit better.
   *
   * Essentially, each time the method is called it returns a different animation index,
   * allowing the rhino to appear like it's moving/
   */
  rhinoMovingAnimation() {
    if (!this.utilityManager.gameOver || !this.caughtSkier) {
      switch (this.rhinoAnimation) {
        default:
          break;
        case 16:
          this.rhinoAnimation = 23;
          break;
        case 23:
          this.rhinoAnimation = 24;
          break;
        case 24:
          this.rhinoAnimation = 16;
          break;
      }
    }
  }

  /**
   * This is exact copy of the jumpSkier method, but with respect to the rhino.
   * Essentially calls the inner method over and over every 300ms, changing index
   * each time from 17 to 23 (to represent the eating animation)
   */
  rhinoEatingAnimation() {
    if (this.caughtSkier) {
      let animationIndex = 17;
      let eatingInterval = setInterval(() => {
        if (animationIndex < 23) {
          this.rhinoAnimation = animationIndex;
        } else {
          clearInterval(eatingInterval);
          this.rhinoAnimation = 16;
          this.utilityManager.gameOver = true;
        }
        animationIndex++;
      }, 300);
    }
  }

  /**
   * Another duplicate of the drawSkier method, with respect to the rhino.
   * There is a buffer variable being added to the x and y coordinates to
   * appear like the rhino is coming towards the user and intersects with him.
   * The buffer decreases by 5 every time the method is called until it reaches 0
   *
   * Once it reaches 0, we try to "sync" the rhino with the skier to aid collision
   */
  drawRhino() {
    if (!this.caughtSkier) {
      this.rhinoMovingAnimation();
    }
    const rhinoAssetName = this.getRhinoAsset();

    const rhinoImage = this.assetManager.loadedAssets[rhinoAssetName];
    const x = (this.graphicsManager.gameWidth - rhinoImage.width) / 2;
    const y = (this.graphicsManager.gameHeight - rhinoImage.height) / 2;

    this.graphicsManager.context.drawImage(
      rhinoImage,
      x + this.rhinoBuffer,
      y + this.rhinoBuffer,
      rhinoImage.width,
      rhinoImage.height
    );
    if (this.rhinoBuffer >= 5) {
      this.rhinoBuffer -= 5;
    } else {
      this.rhinoMapX = this.skierManager.skierMapX;
      this.rhinoMapY = this.skierManager.skierMapY;
    }
  }
}

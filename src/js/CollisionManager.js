export default class CollisionManager {
  constructor() {
    this.obstacleTypes = ['tree', 'treeCluster', 'rock1', 'rock2'];
    this.obstacles = [];

    /**
     * Management files to link together the classes
     */

    this.skierManager = undefined;
    this.rhinoManager = undefined;
    this.assetManager = undefined;
    this.graphicsManager = undefined;
    this.utilityManager = undefined;
  }

  /**
   * Resets the collision manager to default/stock
   */

  resetCollisionManager() {
    this.obstacles = [];
  }

  /**
   * Determines if two items intersect
   * @param r1
   * @param r2
   * @returns {boolean}
   */
  intersectRect(r1, r2) {
    return !(r2.left > r1.right || r2.right < r1.left || r2.top > r1.bottom || r2.bottom < r1.top);
  }

  /**
   *
   * @param minX
   * @param maxX
   * @param minY
   * @param maxY
   * @returns {*}
   */
  calculateOpenPosition(minX, maxX, minY, maxY) {
    const x = _.random(minX, maxX);
    const y = _.random(minY, maxY);

    const foundCollision = _.find(this.obstacles, (obstacle) => {
      return (
        x > obstacle.x - 50 && x < obstacle.x + 50 && y > obstacle.y - 50 && y < obstacle.y + 50
      );
    });

    if (foundCollision) {
      return this.calculateOpenPosition(minX, maxX, minY, maxY);
    } else {
      return {
        x: x,
        y: y
      };
    }
  }

  /**
   * Checks if skier intersected with rhino.
   * Can be refactored (along with the checkIfSkierHitObstacle) to
   * work by taking in skier/rhino object and using that to check
   * against obstacles object or skier/rhino
   */
  checkIfSkierHitRhino() {
    const skierAssetName = this.skierManager.getSkierAsset();
    const skierImage = this.assetManager.loadedAssets[skierAssetName];
    const skierRect = {
      left: this.skierManager.skierMapX + this.graphicsManager.gameWidth / 2,
      right: this.skierManager.skierMapX + skierImage.width + this.graphicsManager.gameWidth / 2,
      top:
        this.skierManager.skierMapY + skierImage.height - 5 + this.graphicsManager.gameHeight / 2,
      bottom: this.skierManager.skierMapY + skierImage.height + this.graphicsManager.gameHeight / 2
    };

    const rhinoAssetName = this.rhinoManager.getRhinoAsset();
    const rhinoImage = this.assetManager.loadedAssets[rhinoAssetName];
    // The rhino collision is sporadic...
    const bufferZone = 2;
    const rhinoRect = {
      left: this.rhinoManager.rhinoMapX + this.graphicsManager.gameWidth / 2 + bufferZone,
      right:
        this.rhinoManager.rhinoMapX +
        rhinoImage.width +
        this.graphicsManager.gameWidth / 2 +
        bufferZone,
      top:
        this.rhinoManager.rhinoMapY +
        rhinoImage.height -
        5 +
        this.graphicsManager.gameHeight / 2 +
        bufferZone,
      bottom:
        this.rhinoManager.rhinoMapY +
        rhinoImage.height +
        this.graphicsManager.gameHeight / 2 +
        bufferZone
    };
    const collision = this.intersectRect(skierRect, rhinoRect);

    if (collision) {
      this.rhinoManager.caughtSkier = true;
      this.skierManager.skierCanMove = false;
      this.rhinoManager.rhinoEatingAnimation();
    }
  }

  /**
   * Checks if skier intersects with an obstacle
   * See above note on checkIfSkierHitRhino
   */
  checkIfSkierHitObstacle() {
    const skierAssetName = this.skierManager.getSkierAsset();
    const skierImage = this.assetManager.loadedAssets[skierAssetName];
    const skierRect = {
      left: this.skierManager.skierMapX + this.graphicsManager.gameWidth / 2,
      right: this.skierManager.skierMapX + skierImage.width + this.graphicsManager.gameWidth / 2,
      top:
        this.skierManager.skierMapY + skierImage.height - 5 + this.graphicsManager.gameHeight / 2,
      bottom: this.skierManager.skierMapY + skierImage.height + this.graphicsManager.gameHeight / 2
    };

    const collision = _.find(this.obstacles, (obstacle) => {
      const obstacleImage = this.assetManager.loadedAssets[obstacle.type];
      const obstacleRect = {
        left: obstacle.x,
        right: obstacle.x + obstacleImage.width,
        top: obstacle.y + obstacleImage.height - 5,
        bottom: obstacle.y + obstacleImage.height
      };

      return this.intersectRect(skierRect, obstacleRect);
    });

    if (collision) {
      /**
       * Need to decrease a life BEFORE the skierDirection
       * changes, otherwise it'll keep growing.
       */
      if (this.skierManager.skierDirection !== 0) {
        this.skierManager.skierLoseLife();
      }
      this.skierManager.skierDirection = 0;
    }
  }
}

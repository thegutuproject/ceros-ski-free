export default class AssetManager {
  constructor() {
    /**
     * Just the assets!
     * @type {{skierDown: string, skierRightDown: string, rock1: string, rock2: string, rhinoDefault: string, treeCluster: string, jumpRamp: string, skierLeft: string, tree: string, rhinoRunLeft1: string, rhinoRunLeft2: string, skierJump5: string, skierJump4: string, skierRight: string, rhinoLiftEat6: string, rhinoLiftEat5: string, skierLeftDown: string, skierJump3: string, rhinoLiftEat2: string, skierJump2: string, rhinoLiftEat1: string, skierJump1: string, rhinoLiftEat4: string, skierCrash: string, rhinoLiftEat3: string}}
     */
    this.assets = {
      skierCrash: 'img/skier_crash.png',
      skierLeft: 'img/skier_left.png',
      skierLeftDown: 'img/skier_left_down.png',
      skierDown: 'img/skier_down.png',
      skierRightDown: 'img/skier_right_down.png',
      skierRight: 'img/skier_right.png',

      skierJump1: 'img/skier_jump_1.png',
      skierJump2: 'img/skier_jump_2.png',
      skierJump3: 'img/skier_jump_3.png',
      skierJump4: 'img/skier_jump_4.png',
      skierJump5: 'img/skier_jump_5.png',

      tree: 'img/tree_1.png',
      treeCluster: 'img/tree_cluster.png',
      rock1: 'img/rock_1.png',
      rock2: 'img/rock_2.png',
      jumpRamp: 'img/jump_ramp.png',

      rhinoDefault: 'img/rhino_default.png',
      rhinoLiftEat1: 'img/rhino_lift_eat_1.png',
      rhinoLiftEat2: 'img/rhino_lift_eat_2.png',
      rhinoLiftEat3: 'img/rhino_lift_eat_3.png',
      rhinoLiftEat4: 'img/rhino_lift_eat_4.png',
      rhinoLiftEat5: 'img/rhino_lift_eat_5.png',
      rhinoLiftEat6: 'img/rhino_lift_eat_6.png',
      rhinoRunLeft1: 'img/rhino_run_left_1.png',
      rhinoRunLeft2: 'img/rhino_run_left_2.png'
    };
    this.loadedAssets = [];
  }

  /**
   * Loads all assets used in the game
   * @returns {*|jQuery}
   */

  loadAssets() {
    const assetPromises = [];

    _.each(this.assets, (asset, assetName) => {
      const assetImage = new Image();
      const assetDeferred = new $.Deferred();

      assetImage.onload = () => {
        assetImage.width /= 2;
        assetImage.height /= 2;

        this.loadedAssets[assetName] = assetImage;
        assetDeferred.resolve();
      };
      assetImage.src = asset;

      assetPromises.push(assetDeferred.promise());
    });

    return $.when.apply($, assetPromises);
  }
}

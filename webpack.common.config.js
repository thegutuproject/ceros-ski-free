const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname),
  entry: {
    app: path.resolve(__dirname, 'src/js/GameManager.js')
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        /**
         * Loads the JS files using Babel with latest
         */
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        /**
         * LESS processor
         */
        test: /\.less$/,
        use: [
          {
            loader: 'style-loader' // creates style nodes from JS strings
          },
          {
            loader: 'css-loader' // translates CSS into CommonJS
          },
          {
            loader: 'less-loader' // compiles Less to CSS
          }
        ]
      }
    ]
  },
  plugins: [
    /**
     * Globally adds lodash and jquery, so we dont have to import it
     */
    new webpack.ProvidePlugin({
      $: 'jquery',
      _: 'lodash'
    }),
    /**
     * Generates the index.html using the original,
     * but adds the bundle.js automagically
     */
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'index.html'),
      filename: path.resolve(__dirname, 'public/index.html'),
      inject: 'body',
      title: 'Rhino Ski'
    }),
    /**
     * Copies the images from the src/img to the public/img
     * Not really sure why I needed this, but I had a difficult
     * time setting up the webserver
     */
    new CopyPlugin([
      {
        from: path.resolve(__dirname, 'src/img'),
        to: path.resolve(__dirname, 'public/img')
      }
    ]),
    new webpack.ProgressPlugin()
  ]
};

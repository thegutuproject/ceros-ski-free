# Ceros Ski Code Challenge


Hey Dan, Samantha and Dovile! Thank you for giving such an enjoying challenge, it was a relief to make something real rather 
than implement some algorithm or sort an array! I have to admit, it took a bit to figure out how the canvas works, and 
it definitely threw in some wrenches in the thinking process, dealing with the loop made it a tad harder than I anticipated!

You can check out the live versions at https://fierce-plains-91197.herokuapp.com/ or http://ceros.thegutuproject.com

**Implementations:**
1. Rhino comes out and eats you (regardless of your skiing skills!) once a certain score is reached
2. Speed is dynamically increased based on time
3. Score multiplier is also dynamically increased based on speed
4. Your score gets added to the leaderboard if you beat the high score (number 1 on leaderboard)
5. Jumping works with the "Space" key
6. Pausing works with the "P" key
7. Bug fixed for game crash on hitting left after a crash
8. Pause menu implemented with buttons
9. Stats section
10. Leaderboard section
11. Play button disables itself when you die, forcing you to press restart
12. Bootstrap for some style! No, I'm kidding i'm not a good designer, but it still looks pretty good.
 
**Bugs:**
1. The animation for skier being eaten by the rhino isn't quite right, something is off.
2. There is a bug (which I don't know how to reproduce yet) where when you restart, the game immediately starts and ends 
as a death, with the menu popping up again. Not sure whats up with that
3. Another bug (seems to happen in tandem with 2) where the leaderboard gets messed up and doubled
4. The skier always starts down-right or straight down, but not down left/
5. The canvas does not adjust to page resizing :(
6. The player stops before the rhino gets to him... still debugging this one

**Missing:**
1. No unit testing. I'm not fully confident in my design choices on how I split up the classes. I might have "set myself 
up for failure" with the structuring and don't want to keep you waiting on the unit tests as well. I'm expecting worse 
case scenario to involve some redesigns
2. Jumping does NOT clear rocks! :/
3. Definitely could be refactored some more

Some of the struggles of this was in the setup believe it or not. Setting up webpack was a new experience, as i've used 
gulp and grunt in the past. It was a steep learning curve, and threw a lot of issues my way. The same can be said for 
the menu, as well as learning canvas. I also ran into some issues with setting the code up, initially I tried doing it 
with named exports but I wasn't really understanding it too well, so I figured i'd go the class route. It didn't occur 
to me to do it with React or Angular, so I just kept it minimal. The collisions system is still something i'm not too 
familiar with, although I understand the rectangle portion, I know it can be overhauled quite well to have the same 
function check obstacles and rhino collisions, but in this case I just copied the method.

MOST Jqeury methods were replaced with standard JS since jquery isn't as popular as it used to be, but some I left in.
Also, bootstrap uses it.

---

Welcome to the Ceros Code Challenge - Ski Edition!

For this challenge, we have included some base code for Ceros Ski, our version of the classic Windows game SkiFree. If
you've never heard of SkiFree, Google has plenty of examples. Better yet, you can play our version here: 
http://ceros-ski.herokuapp.com/  

We understand that everyone has varying levels of free time, so take a look through the requirements below and let us 
know when you will have something for us to look at (we also get to see how well you estimate and manage your time!). 
Previous candidates have taken about 8 hours to complete our challenge. We feel this gives us a clear indicator of your
technical ability and a chance for you to show us how much you give a shit (one of Ceros's core values!) about the position
you're applying for. If anything is unclear, don't hesitate to reach out.

Requirements:
* The base game that we've sent you is not what we would consider production ready code. In fact, it's pretty far from
  it. As part of our development cycle, all code must go through a review. We would like you to perform a review
  on the base code and fix/refactor it. Is the codebase maintainable, unit-testable, and scalable? What design patterns 
  could we use? 
  
  **We will be judging you based upon how you update the code & architecture.**
* There is a bug in the game. Well, at least one bug that we know of. Use the following bug report to debug the code
  and fix it.
  * Steps to Reproduce:
    1. Load the game
    1. Crash into an obstacle
    1. Press the left arrow key
  * Expected Result: The skier gets up and is facing to the left
  * Actual Result: Giant blizzard occurs causing the screen to turn completely white (or maybe the game just crashes!)
* The game's a bit boring as it is. Add a new feature to the game to make it more enjoyable. We've included some ideas for
  you below (or you can come up with your own new feature!). You don't need to do all of them, just pick something to show 
  us you can solve a problem on your own. 
  * Implement jumps. The asset file for jumps is already included. All you gotta do is make the guy jump. We even included
      some jump trick assets if you wanted to get really fancy!
  * Add a score. How will you know that you're the best Ceros Skier if there's no score? Maybe store that score
      somewhere so that it is persisted across browser refreshes.
  * Feed the hungry Rhino. In the original Ski Free game, if you skied for too long, a yeti would chase you
      down and eat you. In Ceros Ski, we've provided assets for a Rhino to catch the skier.
* Update this README file with your comments about your work; what was done, what wasn't, features added & known bugs.
* Provide a way for us to view the completed code and run it, either locally or through a cloud provider
* Be original. Don’t copy someone else’s game implementation!

Bonus:
* Provide a way to reset the game once the game is over
* Provide a way to pause and resume the game
* Skier should get faster as the game progresses
* Deploy the game to a server so that we can play it without setting something up ourselves. We've included a 
  package.json and web.js file that will enable this to run on Heroku. Feel free to use those or use your own code to 
  deploy to a cloud service if you want.
* Write unit tests for your code

And don't think you have to stop there. If you're having fun with this and have the time, feel free to add anything else
you want and show us what you can do! 

We are looking forward to see what you come up with!
